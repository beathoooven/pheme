package de.beathoooven.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhemeServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhemeServerApplication.class, args);
    }

}
