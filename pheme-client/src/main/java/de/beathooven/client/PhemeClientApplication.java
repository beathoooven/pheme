package de.beathooven.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhemeClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhemeClientApplication.class, args);
    }

}
